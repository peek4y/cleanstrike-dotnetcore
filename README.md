# CleanStrike

## Install
- [Dotnet Core](https://www.microsoft.com/net/download)

## Run
 - `dotnet run` or `dotnet run --project CleanStrike.App`

## Test
 - `dotnet test`