using System.Collections.Generic;
using CleanStrike.Core.Models;
using CleanStrike.Core.Models.Actions;

namespace CleanStrike.App
{
    public class PlayOption
    {
        public string Id { get; set; }
        public IPlayAction Action { get; set; }

        public static List<PlayOption> GetPlayOptions()
        {
            return new List<PlayOption> {
                new PlayOption {
                    Id = "1",
                    Action = new StrikeAction()
                },
                new PlayOption {
                    Id = "2",
                    Action = new MultiStrikeAction()
                },
                new PlayOption {
                    Id = "3",
                    Action = new RedStrikeAction()
                },
                new PlayOption {
                    Id = "4",
                    Action = new StrikerStrikeAction()
                },
                new PlayOption {
                    Id = "5",
                    Action = new DefunctCoinAction()
                },
                new PlayOption {
                    Id = "6",
                    Action = null
                }
            };
        }
    }
}