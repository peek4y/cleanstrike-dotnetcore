﻿using System;
using System.Collections.Generic;
using System.Linq;
using CleanStrike.Core.Models;

namespace CleanStrike.App
{
    class Program
    {
        static void Main(string[] args)
        {
            IBoard board = new Board();
            IPlayer playerOne = new Player("Player 1", board);
            IPlayer playerTwo = new Player("Player 2", board);
            List<PlayOption> playOptions = PlayOption.GetPlayOptions();

            while (!board.IsGameOver())
            {
                HandlePlayerOption(playOptions, playerOne);
                HandlePlayerOption(playOptions, playerTwo);
            }
            IPlayer winner = playerOne.GetScore() > playerTwo.GetScore()
            ? playerOne : playerTwo;

            Console.WriteLine("Game Over!");
            Console.WriteLine("ScoreCard: ");
            Console.WriteLine($"[{playerOne.GetName()}]: {playerOne.GetScore()} points");
            Console.WriteLine($"[{playerTwo.GetName()}]: {playerTwo.GetScore()} points");
            Console.WriteLine($"Winner is: {winner.GetName()}");

        }
        public static void HandlePlayerOption(
            List<PlayOption> options,
            IPlayer player)
        {
            PrintOptions(player, options);
            Console.Write("> ");
            string optionId = Console.ReadLine() ?? "";
            PlayOption selectedOption = options.Where(option => option.Id == optionId).FirstOrDefault();
            if (selectedOption == null)
            {
                Console.WriteLine("Invalid Option.");
                return;
            }
            if (selectedOption.Action == null) return;
            player.Play(selectedOption.Action);
            Console.WriteLine("");
        }
        public static void PrintOptions(IPlayer player, List<PlayOption> playOptions)
        {
            Console.WriteLine($"{player.GetName()}: Choose an outcome from the list below");
            playOptions
            .ForEach(option => Console.WriteLine($"{option.Id}.  {option.Action?.GetName() ?? "None"}"));
        }
    }
}
