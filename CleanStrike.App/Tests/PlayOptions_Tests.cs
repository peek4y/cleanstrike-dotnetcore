using System.Linq;
using CleanStrike.Core.Models.Actions;
using Xunit;

namespace CleanStrike.App.Tests
{
    public class PlayOptions_Tests
    {
        [Fact]
        public void MustReturnProperOptionsList()
        {
            Assert.Equal(6, PlayOption.GetPlayOptions().Count);
        }

        [Fact]
        public void MustContainProperPlayActions()
        {
            var playOptions = PlayOption.GetPlayOptions();

            playOptions
            .Where(option => option.Action != null)
            .ToList()
            .ForEach(option => Assert.IsAssignableFrom<BaseAction>(option.Action));
        }
    }
}