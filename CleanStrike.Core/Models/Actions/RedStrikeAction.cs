namespace CleanStrike.Core.Models.Actions
{
    public class RedStrikeAction : BaseAction, IPlayAction
    {
        public override string Name => "Red Strike";
        public override int Points => 3;

        public override int CoinChange => -1;
    }
}