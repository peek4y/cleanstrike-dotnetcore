namespace CleanStrike.Core.Models
{
    public interface IPlayAction
    {
        string GetName();
        int GetPoints();

        int GetCoinChange();
    }
}