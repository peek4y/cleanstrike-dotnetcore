using System;

namespace CleanStrike.Core.Models.Actions
{
    public class StrikerStrikeAction : BaseAction, IPlayAction
    {
        public override string Name => "Striker Strike";
        public override int Points => -1;

        public override int CoinChange => new Random().Next(0, 2) - 1;
    }
}