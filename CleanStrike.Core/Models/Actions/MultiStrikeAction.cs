namespace CleanStrike.Core.Models.Actions
{
    public class MultiStrikeAction : BaseAction, IPlayAction
    {
        public override string Name => "Multi Strike";
        public override int Points => 2;

        public override int CoinChange => -2;
    }
}