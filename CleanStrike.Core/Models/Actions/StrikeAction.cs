namespace CleanStrike.Core.Models.Actions
{
    public class StrikeAction : BaseAction, IPlayAction
    {
        public override string Name => "Strike";
        public override int Points => 1;

        public override int CoinChange => -1;
    }
}