namespace CleanStrike.Core.Models.Actions
{
    public class DefunctCoinAction : BaseAction, IPlayAction
    {
        public override string Name => "Defunct Coin";
        public override int Points => -2;

        public override int CoinChange => -1;
    }
}