namespace CleanStrike.Core.Models.Actions
{
    public abstract class BaseAction : IPlayAction
    {
        public abstract string Name { get; }
        public abstract int Points { get; }
        public abstract int CoinChange { get; }

        public int GetPoints() => this.Points;

        public int GetCoinChange() => this.CoinChange;

        public string GetName() => this.Name;
    }

}