using System;

namespace CleanStrike.Core.Models
{
    public class Player : IPlayer
    {
        private readonly IBoard _board;
        private readonly string _name;

        private int _score = 0;

        public Player(string Name, IBoard board)
        {
            this._board = board;
            this._name = Name;
        }


        public void Play(IPlayAction action)
        {
            if (this._board.IsGameOver()) return;
            this._board.UpdateBoard(action);
            this._score += action.GetPoints();
        }

        public int GetScore() => this._score;

        public string GetName() => this._name;
    }
}