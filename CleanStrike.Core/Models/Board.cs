namespace CleanStrike.Core.Models
{
    public class Board : IBoard
    {
        private int _coinsCount = 10;

        private bool _redPocketed = false;

        public int GetCoinsCount() => this._coinsCount;

        public bool IsRedPocketed() => this._redPocketed;

        public bool IsGameOver() => this._coinsCount == 0;

        public void UpdateBoard(IPlayAction action)
        {
            this._coinsCount = this._coinsCount + (
                (this._redPocketed && (action is Actions.RedStrikeAction)) ?
                0 : action.GetCoinChange()
            );
            this._redPocketed = (action is Actions.RedStrikeAction && !this._redPocketed) ?
            true : this._redPocketed;
        }
    }
}