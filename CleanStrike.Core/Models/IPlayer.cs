namespace CleanStrike.Core.Models
{
    public interface IPlayer
    {
        void Play(IPlayAction action);
        string GetName();
        int GetScore();
    }
}