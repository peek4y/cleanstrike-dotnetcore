namespace CleanStrike.Core.Models
{
    public interface IBoard
    {
        bool IsGameOver();
        void UpdateBoard(IPlayAction action);

        int GetCoinsCount();
    }
}