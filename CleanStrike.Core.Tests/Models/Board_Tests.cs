using System.Linq;
using CleanStrike.Core.Models;
using CleanStrike.Core.Models.Actions;
using Xunit;

namespace CleanStrike.Core.Tests.Models
{
    public class Board_Tests
    {
        [Fact]
        public void BoardMustInitialize9Coins()
        {
            IBoard board = new Board();
            Assert.Equal(10, board.GetCoinsCount());
        }

        [Fact]
        public void BoardMustReflectCoinChangeOnAction()
        {
            IBoard board = new Board();
            var initialCount = board.GetCoinsCount();
            board.UpdateBoard(new StrikeAction());
            Assert.NotEqual(initialCount, board.GetCoinsCount());
        }

        [Fact]
        public void BoardMustReturnProperGameOverStatus()
        {
            IBoard board = new Board();

            Enumerable
            .Range(0, board.GetCoinsCount())
            .ToList()
            .ForEach((i) => board.UpdateBoard(new StrikeAction()));

            Assert.True(board.IsGameOver(), "Looks like the game isn't over yet?");
        }

        [Fact]
        public void MustNotAllowDuplicateRedStrikeAction()
        {
            IBoard board = new Board();

            var initialCount = board.GetCoinsCount();
            board.UpdateBoard(new RedStrikeAction());
            Assert.Equal(initialCount - 1, board.GetCoinsCount());
            board.UpdateBoard(new RedStrikeAction());
            Assert.Equal(initialCount - 1, board.GetCoinsCount());
        }
    }
}