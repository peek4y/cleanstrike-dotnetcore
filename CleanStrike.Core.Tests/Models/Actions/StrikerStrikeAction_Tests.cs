using CleanStrike.Core.Models.Actions;
using Xunit;

namespace CleanStrike.Core.Tests.Models.Actions
{
    public class StrikerStrikeAction_Tests
    {
        [Fact]
        public void AssertStrikerStrikeActionData()
        {
            var action = new StrikerStrikeAction();
            Assert.Equal(-1, action.GetPoints());
            Assert.InRange(action.GetCoinChange(), -1, 0);
        }
    }
}