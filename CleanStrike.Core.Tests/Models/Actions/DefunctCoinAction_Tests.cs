using CleanStrike.Core.Models.Actions;
using Xunit;

namespace CleanStrike.Core.Tests.Models.Actions
{
    public class DefunctCoinAction_Tests
    {
        [Fact]
        public void AssertDefunctCoinActionData() 
        {
            var action = new DefunctCoinAction();
            Assert.Equal(-2, action.GetPoints());
            Assert.Equal(-1, action.GetCoinChange());
        }
    }
}